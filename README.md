# README #

MTM ATM REST API

### Prerequisites ###
* Gradle
* Java 8+
### Set up ###
* import gradle project
* run tests with: gradle test
* start app: gradlew bootRun

# Endpoints

## 	Health check
### Request

`GET http://localhost:8080/`

### Response

API Health is ok.

## 	Deposit Money
### Request

`PUT http://localhost:8080/money`
`{
    "1":100,
    "10":70,
    "50":73,
    "100":200,
    "200":62,
    "500":30
}`
### Response
`{
    "message": "The ATM balance is 51850."
}`

## 	Withdraw Money
### Request

`POST http://localhost:8080/money?amount=7300`

### Response
`{
    "500": 14,
    "200": 1,
    "100": 1
}`

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact