package ro.mtm.atm.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.mtm.atm.domain.ATM;

import java.util.*;

import static java.util.Collections.reverseOrder;
import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;
import static ro.mtm.atm.constant.AtmConstants.*;

@Slf4j
@Service
public class AtmServiceImpl implements AtmService {

    private static ATM atmRepository = ATM.getInstance();

    public int getTotalAmount() {
        return atmRepository.getTotalAmount();
    }

    public HashMap<String, ?> deposit(HashMap<String, Integer> input) {
        Optional<String> validationError = validateDepositInput(input);
        return validationError.isPresent()
                ? getErrorResultMap(validationError.get())
                : addMoney(input);
    }

    public HashMap<String, ?> withdraw(int amountToWithdraw) {
        Optional<String> validationError = validateWithdrawInput(amountToWithdraw);
        return validationError.isPresent()
                ? getErrorResultMap(validationError.get())
                : takeMoney(amountToWithdraw);
    }

    public HashMap<String, Object> addMoney(HashMap<String, Integer> map) {
        atmRepository.setTotalAmount(getNewTotalAmount(map));
        atmRepository.setAvailableNotes(getNewAvailableNotes(map));
        return getResultMap(MESSAGE, ATM_BALANCE_MESSAGE + atmRepository.getTotalAmount() + ".");
    }

    private LinkedHashMap<String, Integer> getNewAvailableNotes(HashMap<String, Integer> map) {
        if (atmRepository.getAvailableNotes().isEmpty()){
            return getSortedAvailableNotesMap(map);
        } else {
            LinkedHashMap<String, Integer> newAvailableNotes = new LinkedHashMap<>();
            getSortedAvailableNotesMap(map).forEach((k, v) -> newAvailableNotes.merge(k, v, Integer::sum));
            return newAvailableNotes;
        }
    }

    private int getNewTotalAmount(HashMap<String, Integer> map) {
        int newAddedAmount = getTotalAmountFromDeposit(map);
        int currentTotalAmount = atmRepository.getTotalAmount();
        if (currentTotalAmount != 0) {
            return newAddedAmount + currentTotalAmount;
        } else {
            return newAddedAmount;
        }
    }

    public HashMap<String, ?> takeMoney(int amount) {
        HashMap<String, Integer> solutionMap = new HashMap<>();
        HashMap<String, String> errorMap = new HashMap<>();
        LinkedHashMap<String, Integer> availableNotes = atmRepository.getAvailableNotes();
        int totalAmount = atmRepository.getTotalAmount();

        for (Map.Entry<String, Integer> entry : availableNotes.entrySet()) {
            if (entry.getValue() == 0) continue;
            int oneBill = Integer.parseInt(entry.getKey());
            if (amount - oneBill < 0) continue;
            else if (amount - oneBill == 0) {
                amount = 0;
                solutionMap.put(entry.getKey(), 1);
                availableNotes.put(entry.getKey(), entry.getValue() - 1);
                totalAmount -= oneBill;
                break;
            } else if (amount - oneBill > 0) {
                int numberOfBills = 0;
                int tempAmount = amount;
                while (tempAmount > 0 && numberOfBills <= entry.getValue()) {
                    tempAmount -= oneBill;
                    numberOfBills++;
                }
                if (tempAmount == 0) {
                    amount = tempAmount;
                    solutionMap.put(entry.getKey(), numberOfBills);
                    availableNotes.put(entry.getKey(), entry.getValue() - numberOfBills);
                    totalAmount -= oneBill * numberOfBills;
                    break;
                } else {
                    while (numberOfBills > entry.getValue() + 1) numberOfBills--;
                    amount -= oneBill * --numberOfBills;
                    solutionMap.put(entry.getKey(), numberOfBills);
                    availableNotes.put(entry.getKey(), entry.getValue() - numberOfBills);
                    totalAmount -= oneBill * numberOfBills;
                    continue;
                }
            }
        }
        if (amount > 0) {
            errorMap.put(ERROR, NOT_ENOUGH_NOTES_TO_SERVE_REQUEST);
            return errorMap;
        }
        atmRepository.setAvailableNotes(availableNotes);
        atmRepository.setTotalAmount(totalAmount);
        return getSortedAvailableNotesMap(solutionMap);
    }

    private HashMap<String, Object> getResultMap(String key, String value) {
        HashMap<String, Object> resultMap = new HashMap<>();
        resultMap.put(key, value);
        return resultMap;
    }

    private HashMap<String, ?> getErrorResultMap(String validationError) {
        return getResultMap(ERROR, validationError);
    }

    protected LinkedHashMap<String, Integer> getSortedAvailableNotesMap(HashMap<String, Integer> map) {
        Comparator<String> withRealValue = Comparator.comparingInt(Integer::parseInt);
        return map.entrySet()
                .stream()
                .sorted(reverseOrder(comparingByKey(withRealValue)))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
    }

    protected int getTotalAmountFromDeposit(HashMap<String, Integer> map) {
        return map.entrySet().stream().mapToInt((entry) -> Integer.parseInt(entry.getKey()) * entry.getValue()).sum();
    }

    public static Optional<String> validateDepositInput(HashMap<String, Integer> map) {
        if (map.isEmpty()) {
            return Optional.of(WRONG_REQUEST_FORMAT_EMPTY_REQUEST);
        }
        int actualAmountToDeposit = 0;
        Optional<String> errorReponse = Optional.empty();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (entry.getValue() == null) {
                return Optional.of(WRONG_REQUEST_FORMAT_NULL_VALUE);
            }
            if (entry.getValue() > MAXIMUM_VALUE_FOR_ENTRY) {
                return Optional.of(ENTRY_VALUE_EXCEEDS_LIMIT + entry.getKey() + "\":" + entry.getValue());
            }
            if (!isNumeric(entry.getKey())) {
                return Optional.of(WRONG_REQUEST_FORMAT_INVALID_KEY);
            }
            if (Integer.parseInt(entry.getKey()) > MAXIMUM_DENOMINATION_FOR_ENTRY) {
                return Optional.of(ENTRY_KEY_EXCEEDS_LIMIT + entry.getKey() + "\":" + entry.getValue());
            }
            actualAmountToDeposit += Integer.parseInt(entry.getKey()) * entry.getValue();
        }
        if (actualAmountToDeposit > MAXIMUM_ATM_CAPACITY) {
            return Optional.of(MAXIMUM_ATM_CAPACITY_SURPASSED + Math.abs(MAXIMUM_ATM_CAPACITY - actualAmountToDeposit));
        }
        if ((actualAmountToDeposit + atmRepository.getTotalAmount()) > MAXIMUM_ATM_CAPACITY) {
            return Optional.of(MAXIMUM_ATM_CAPACITY_SURPASSED + Math.abs(MAXIMUM_ATM_CAPACITY - (actualAmountToDeposit + atmRepository.getTotalAmount())));
        }
        return errorReponse;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            int d = Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Optional<String> validateWithdrawInput(int amount) {
        if (amount == 0) {
            return Optional.of(WRONG_REQUEST_FORMAT_ZERO_WITHDRAWED_AMOUNT);
        }
        if (amount > MAXIMUM_ATM_CAPACITY) {
            return Optional.of(WRONG_REQUEST_FORMAT_EXCEEDING_WITHDRAWED_AMOUNT);
        }
        if (amount > atmRepository.getTotalAmount()) {
            return Optional.of(WITHDRAWED_AMOUNT_EXCEEDS_ATM_BALANCE);
        }
        return Optional.empty();
    }
}
