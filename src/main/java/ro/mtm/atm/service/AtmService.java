package ro.mtm.atm.service;

import java.util.HashMap;

public interface AtmService {
    int getTotalAmount();
    HashMap<String, ?> deposit(HashMap<String, Integer> input);
    HashMap<String, ?> withdraw(int amountToWithdraw);
}
