package ro.mtm.atm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.mtm.atm.service.AtmService;

import java.util.HashMap;

import static ro.mtm.atm.constant.AtmConstants.ERROR;

@RestController
public class AtmController {

    @Autowired
    AtmService atmService;

    @GetMapping(path = "/")
    public String index() {
        return "API Health is ok.";
    }

    @PutMapping(path = "/money")
    public ResponseEntity<?> depositMoney(@RequestBody HashMap<String, Integer> map) {
        HashMap<String, ?> response = atmService.deposit(map);
        return response.get(ERROR) != null
                ? new ResponseEntity<>(response, HttpStatus.BAD_REQUEST)
                : new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(path = "/money")
    public ResponseEntity<?> withdrawMoney(@RequestParam int amount) {
        HashMap<String, ?> response = atmService.withdraw(amount);
        return response.get(ERROR) != null
                ? new ResponseEntity<>(response, HttpStatus.BAD_REQUEST)
                : new ResponseEntity<>(response, HttpStatus.OK);
    }
}
