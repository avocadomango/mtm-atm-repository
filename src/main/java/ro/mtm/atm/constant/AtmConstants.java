package ro.mtm.atm.constant;

public class AtmConstants {
    public static final int MAXIMUM_ATM_CAPACITY = 100000;
    public static final int MAXIMUM_VALUE_FOR_ENTRY = 100000;
    public static final int MAXIMUM_DENOMINATION_FOR_ENTRY = 1000;

    public static final String MESSAGE = "message";
    public static final String ERROR = "error";
    public static final String ATM_BALANCE_MESSAGE = "The ATM balance is ";

    public static final String WRONG_REQUEST_FORMAT_NULL_VALUE = "Wrong request format. The value should not be null.";
    public static final String ENTRY_VALUE_EXCEEDS_LIMIT = "Value exceeds maximum allowed amount in the following entry: \"";
    public static final String WRONG_REQUEST_FORMAT_INVALID_KEY = "Wrong request format. The key should be the string representation of a number.";
    public static final String ENTRY_KEY_EXCEEDS_LIMIT = "Denomination exceeds maximum allowed amount in the following entry: \"";
    public static final String MAXIMUM_ATM_CAPACITY_SURPASSED = "Maximum capacity of ATM surpassed by ";
    public static final String WRONG_REQUEST_FORMAT_ZERO_WITHDRAWED_AMOUNT = "Wrong request format. The withdrawed ammount cannot be 0.";
    public static final String WRONG_REQUEST_FORMAT_EXCEEDING_WITHDRAWED_AMOUNT = "Wrong request format. The withdrawed ammount cannot exceed 100000.";
    public static final String WITHDRAWED_AMOUNT_EXCEEDS_ATM_BALANCE = "The withdrawed amount exceeds ATM current balance.";
    public static final String WRONG_REQUEST_FORMAT_EMPTY_REQUEST = "Wrong request format. The request cannot be empty.";
    public static final String NOT_ENOUGH_NOTES_TO_SERVE_REQUEST = "Requested amount cannot pe processed.";
}
