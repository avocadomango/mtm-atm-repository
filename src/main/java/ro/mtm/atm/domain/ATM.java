package ro.mtm.atm.domain;

import java.util.LinkedHashMap;

public class ATM {

    private static final ATM instance = new ATM();
    private ATM() {
    }
    public static ATM getInstance() {
        return instance;
    }

    private LinkedHashMap<String, Integer> availableNotes = new LinkedHashMap<>();
    private int totalAmount = 0;

    public int getTotalAmount() {
        return totalAmount;
    }
    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }
    public LinkedHashMap<String, Integer> getAvailableNotes() {
        return availableNotes;
    }
    public void setAvailableNotes(LinkedHashMap<String, Integer> availableNotes) {
        this.availableNotes = availableNotes;
    }
    public void wipeDataForTestingPurposes() {
        this.availableNotes = new LinkedHashMap<>();
        this.totalAmount = 0;
    }
}
