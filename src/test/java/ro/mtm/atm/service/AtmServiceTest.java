package ro.mtm.atm.service;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ro.mtm.atm.domain.ATM;
import ro.mtm.atm.util.TestingUtils;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.*;
import static ro.mtm.atm.constant.AtmConstants.*;

@SpringBootTest
public class AtmServiceTest {

    @Autowired
    AtmServiceImpl atmService;

    @BeforeEach
    void init() {
        ATM.getInstance().wipeDataForTestingPurposes();
    }

    @Test
    void test_get_total_amount_from_deposit() {
        JSONObject test_input = TestingUtils.getSampleJSON();
        int expectedAmount = 100 + 10 * 70 + 50 * 73 + 100 * 200 + 200 * 62 + 30 * 500;

        int result = atmService.getTotalAmountFromDeposit(test_input);

        assertEquals(result, expectedAmount);
    }

    @Test
    void test_get_sorted_available_notes_from_deposit() {
        JSONObject test_input = TestingUtils.getSampleJSON();

        HashMap result = atmService.getSortedAvailableNotesMap(test_input);

        assertEquals(TestingUtils.getSortedSampleJSON(),
                result);
    }

    @Test
    void test_deposit_with_example_json() {
        JSONObject test_input = TestingUtils.getSampleJSON();
        int expectedAmount = 51850;

        HashMap result = atmService.deposit(test_input);

        assertEquals(ATM_BALANCE_MESSAGE + expectedAmount + ".",
                result.get(MESSAGE));
    }


    @Test
    void test_multiple_deposit_with_eventually_exceeding_atm_capactiy_json() {
        JSONObject test_input = TestingUtils.getSampleJSON();
        int exceededAmount = Math.abs(MAXIMUM_ATM_CAPACITY - 51850 * 2);

        HashMap firstDeposit = atmService.deposit(test_input);
        HashMap secondDeposit = atmService.deposit(test_input);

        assertNull(firstDeposit.get(ERROR));
        assertNotNull(secondDeposit.get(ERROR));
        assertTrue(secondDeposit.get(ERROR).toString().contains(MAXIMUM_ATM_CAPACITY_SURPASSED + exceededAmount));
    }


    @Test
    void test_withdraw_with_example_json() {
        JSONObject test_input = TestingUtils.getSampleJSON();
        atmService.deposit(test_input);
        int amountBefore = atmService.getTotalAmount();

        HashMap result = atmService.withdraw(512);
        int amountNow = atmService.getTotalAmount();

        assertNull(result.get(ERROR));
        assertEquals(amountBefore - 512, amountNow);
    }

    @Test
    void test_withdraw_when_not_enough_bills() {
        JSONObject test_input = TestingUtils.getSampleJSONWithNotSoManyBills();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(517);

        assertNotNull(result.get(ERROR));
        assertTrue(result.get(ERROR).toString()
                .contains(NOT_ENOUGH_NOTES_TO_SERVE_REQUEST));
    }

    @Test
    void test_withdraw_when_just_enough_bills() {
        JSONObject test_input = TestingUtils.getSampleJSONWithJustEnoughBills();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(547);

        assertNull(result.get(ERROR));
        assertEquals(TestingUtils.getResultJSONWithJustEnoughBills(),
                result);
    }

    @Test
    void test_withdraw_when_just_enough_bills_but_different() {
        JSONObject test_input = TestingUtils.getSampleJSONWithNotSoManyBillsButDiverse();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(547);

        assertNotNull(result.get(ERROR));
        assertTrue(result.get(ERROR).toString()
                .contains(NOT_ENOUGH_NOTES_TO_SERVE_REQUEST));
    }

    @Test
    void test_withdraw_when_enough_money_but_not_enough_bills() {
        JSONObject test_input = TestingUtils.getSampleJSONWithEnoughMoneyButNotManyBills();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(547);

        assertNotNull(result.get(ERROR));
        assertTrue(result.get(ERROR).toString()
                .contains(NOT_ENOUGH_NOTES_TO_SERVE_REQUEST));
    }

    @Test
    void test_withdraw_when_enough_money_and_even_fewer_bills() {
        JSONObject test_input = TestingUtils.getSampleJSONWithEnoughMoneyAndEvenFewerBills();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(1234);

        assertNotNull(result.get(ERROR));
        assertTrue(result.get(ERROR).toString()
                .contains(NOT_ENOUGH_NOTES_TO_SERVE_REQUEST));
    }

    @Test
    void test_withdraw_when_big_amount_but_small_denominations() {
        JSONObject test_input = TestingUtils.getSampleJSONWithSmallDenominations();
        atmService.deposit(test_input);

        HashMap result = atmService.withdraw(25100);

        assertNull(result.get(ERROR));
        assertFalse(result.isEmpty());
        assertNotNull(result);
    }

    @Test
    void test_withdraw_multiple_times() {
        JSONObject test_input = TestingUtils.getSampleJSON();
        //51850
        atmService.deposit(test_input);

        assertEquals(51850, atmService.getTotalAmount());
        HashMap result = atmService.withdraw(7300);
        assertEquals(44550, atmService.getTotalAmount());
        HashMap result2 = atmService.withdraw(7300);
        assertEquals(37250, atmService.getTotalAmount());
        HashMap result3 = atmService.withdraw(7300);
        assertEquals(29950, atmService.getTotalAmount());
        HashMap result4 = atmService.withdraw(7300);
        assertEquals(22650, atmService.getTotalAmount());
        HashMap result5 = atmService.withdraw(7300);
        assertEquals(15350, atmService.getTotalAmount());
        HashMap result6 = atmService.withdraw(7300);
        assertEquals(8050, atmService.getTotalAmount());
        HashMap result7 = atmService.withdraw(7300);
        assertEquals(750, atmService.getTotalAmount());
        HashMap result8 = atmService.withdraw(7300);
        assertEquals(750, atmService.getTotalAmount());

        assertNull(result.get(ERROR));
        assertFalse(result.isEmpty());
        assertNotNull(result);
    }


    @Test
    void test_sorted_deposit_with_example_json() {
        JSONObject test_input = TestingUtils.getSampleJSON();

        atmService.deposit(test_input);

        assertEquals(TestingUtils.getSortedSampleJSON(),
                ATM.getInstance().getAvailableNotes());
    }
}
