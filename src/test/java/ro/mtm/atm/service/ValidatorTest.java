package ro.mtm.atm.service;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ro.mtm.atm.domain.ATM;
import ro.mtm.atm.util.TestingUtils;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static ro.mtm.atm.constant.AtmConstants.*;

@SpringBootTest
public class ValidatorTest {
    @Test
    void test_deposit_exceeding_atm_capacity_validation_message() {
        JSONObject badInput = TestingUtils.getExceedingAtmCapacitySampleJSON();

        Optional totalAmountValidationError = AtmServiceImpl.validateDepositInput(badInput);

        assertTrue(totalAmountValidationError.isPresent());
        assertTrue(totalAmountValidationError.get().toString().contains(MAXIMUM_ATM_CAPACITY_SURPASSED));
    }
    @Test
    void test_deposit_exceeding_value_for_entry() {
        JSONObject badInput = TestingUtils.getExceedingAmountForParameterCapacity();

        Optional valueValidationError = AtmServiceImpl.validateDepositInput(badInput);

        assertTrue(valueValidationError.isPresent());
        assertTrue(valueValidationError.get().toString().contains(ENTRY_VALUE_EXCEEDS_LIMIT));
    }
    @Test
    void test_deposit_exceeding_denomination_for_entry() {
        JSONObject badInput = TestingUtils.getExceedingDenominationForParameterCapacity();

        Optional denominationValidationError = AtmServiceImpl.validateDepositInput(badInput);

        assertTrue(denominationValidationError.isPresent());
        assertTrue(denominationValidationError.get().toString().contains(ENTRY_KEY_EXCEEDS_LIMIT));
    }

    @Test
    void test_deposit_with_illegal_character_as_key() {
        JSONObject badInput = TestingUtils.getIllegalCharacterJSON();

        Optional illegalCharacterValidationError = AtmServiceImpl.validateDepositInput(badInput);

        assertTrue(illegalCharacterValidationError.isPresent());
        assertEquals(WRONG_REQUEST_FORMAT_INVALID_KEY,
                illegalCharacterValidationError.get());
    }

    @Test
    void test_withdraw_with_zero_amount() {
        int withdrawedAmount = 0;

        Optional invalidWithdrawalValidationError = AtmServiceImpl.validateWithdrawInput(withdrawedAmount);

        assertTrue(invalidWithdrawalValidationError.isPresent());
        assertEquals(WRONG_REQUEST_FORMAT_ZERO_WITHDRAWED_AMOUNT,
                invalidWithdrawalValidationError.get());
    }

    @Test
    void test_withdraw_with_exceeding_atm_capacity() {
        int exceedingWhithdrawedAmount = MAXIMUM_ATM_CAPACITY + 1;

        Optional exceedingAtmCapacityValidationError = AtmServiceImpl.validateWithdrawInput(exceedingWhithdrawedAmount);

        assertTrue(exceedingAtmCapacityValidationError.isPresent());
        assertEquals(WRONG_REQUEST_FORMAT_EXCEEDING_WITHDRAWED_AMOUNT,
                exceedingAtmCapacityValidationError.get());
    }

    @Test
    void test_withdraw_with_exceeding_atm_available_amount() {
        ATM.getInstance().setTotalAmount(1500);
        int amountThatExceedsTotalAmount = 1501;

        Optional exceedingAtmCapacityValidationError = AtmServiceImpl.validateWithdrawInput(amountThatExceedsTotalAmount);

        assertTrue(exceedingAtmCapacityValidationError.isPresent());
        assertEquals(WITHDRAWED_AMOUNT_EXCEEDS_ATM_BALANCE,
                exceedingAtmCapacityValidationError.get());
    }

}
