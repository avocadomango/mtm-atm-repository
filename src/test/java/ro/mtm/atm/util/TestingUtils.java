package ro.mtm.atm.util;

import org.json.simple.JSONObject;

public class TestingUtils {
    public static JSONObject getSampleJSON() {
        JSONObject json = new JSONObject();
        json.put("1", 100);
        json.put("10", 70);
        json.put("50", 73);
        json.put("100", 200);
        json.put("200", 62);
        json.put("500", 30);
        System.out.println("getSampleJSON" + json.toJSONString());
        return json;
    }

    public static JSONObject getSortedSampleJSON() {
        JSONObject json = new JSONObject();
        json.put("500", 30);
        json.put("200", 62);
        json.put("100", 200);
        json.put("50", 73);
        json.put("10", 70);
        json.put("1", 100);
        System.out.println("getSortedSampleJSON" + json.toJSONString());
        return json;
    }
    public static JSONObject getSampleJSONWithNotSoManyBills() {
        JSONObject json = new JSONObject();
        json.put("1", 2);
        json.put("10", 4);
        json.put("50", 3);
        json.put("100", 1);
        json.put("200", 5);
        json.put("500", 8);
        System.out.println("getSampleJSONWithNotSoManyBills" + json.toJSONString());
        return json;
    }
    public static JSONObject getExceedingAtmCapacitySampleJSON() {
        JSONObject json = new JSONObject();
        json.put("1", 100);
        json.put("10", 70);
        json.put("50", 73);
        json.put("100", 200);
        json.put("200", 62);
        json.put("500", 3000);
        System.out.println("getExceedingAtmCapacitySampleJSON" + json.toJSONString());
        return json;
    }

    public static JSONObject getExceedingAmountForParameterCapacity() {
        JSONObject json = new JSONObject();
        json.put("1", 100001);
        System.out.println("getExceedingAmountForParameterCapacity" + json.toJSONString());
        return json;
    }

    public static JSONObject getExceedingDenominationForParameterCapacity() {
        JSONObject json = new JSONObject();
        json.put("1", 100);
        json.put("10", 70);
        json.put("50", 73);
        json.put("100", 200);
        json.put("200", 62);
        json.put("5000", 30);
        System.out.println("getExceedingDenominationForParameterCapacity" + json.toJSONString());
        return json;
    }

    public static JSONObject getIllegalCharacterJSON() {
        JSONObject json = new JSONObject();
        json.put("a", 100);
        json.put("10", 70);
        json.put("50", 73);
        json.put("100", 200);
        json.put("200", 62);
        json.put("500", 30);
        System.out.println("getIllegalCharacterJSON" + json.toJSONString());
        return json;
    }

    public static JSONObject getSampleJSONWithNotSoManyBillsButDiverse() {
        JSONObject json = new JSONObject();
        json.put("1", 2);
        json.put("10", 4);
        json.put("50", 3);
        json.put("100", 1);
        json.put("200", 5);
        json.put("500", 8);
        System.out.println("getSampleJSONWithNotSoManyBillsButDiverse" + json.toJSONString());
        return json;
    }

    public static JSONObject getSampleJSONWithEnoughMoneyButNotManyBills() {
        JSONObject json = new JSONObject();
        json.put("1", 7);
        json.put("50", 30);
        json.put("100", 10);
        json.put("200", 20);
        json.put("500", 10);
        System.out.println("getSampleJSONWithEnoughMoneyButNotManyBills" + json.toJSONString());
        return json;
    }

    public static JSONObject getSampleJSONWithJustEnoughBills() {
        JSONObject json = new JSONObject();
        json.put("1", 7);
        json.put("5", 4);
        json.put("10", 2);
        json.put("50", 3);
        json.put("100", 1);
        json.put("200", 5);
        json.put("500", 8);
        System.out.println("getSampleJSONWithJustEnoughBills" + json.toJSONString());
        return json;
    }

    public static JSONObject getResultJSONWithJustEnoughBills() {
        JSONObject json = new JSONObject();
        json.put("500", 1);
        json.put("10", 2);
        json.put("5", 4);
        json.put("1", 7);
        System.out.println("getResultJSONWithJustEnoughBills" + json.toJSONString());
        return json;
    }

    public static JSONObject getSampleJSONWithEnoughMoneyAndEvenFewerBills() {
        JSONObject json = new JSONObject();
        json.put("50", 30);
        json.put("100", 10);
        json.put("200", 20);
        json.put("500", 10);
        System.out.println("getSampleJSONWithEnoughMoneyAndEvenFewerBills" + json.toJSONString());
        return json;
    }

    public static JSONObject getSampleJSONWithSmallDenominations() {
        JSONObject json = new JSONObject();
        json.put("50", 10);
        json.put("10", 4000);
        json.put("5", 4000);
        json.put("1", 5000);
        System.out.println("getSampleJSONWithSmallDenominations" + json.toJSONString());
        return json;
    }
}
